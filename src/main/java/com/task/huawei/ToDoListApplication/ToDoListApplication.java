package com.task.huawei.ToDoListApplication;

import com.task.huawei.ToDoListApplication.model.Task;
import com.task.huawei.ToDoListApplication.model.User;
import com.task.huawei.ToDoListApplication.repository.TestTaskRepository;
import com.task.huawei.ToDoListApplication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDateTime;
import java.util.List;
import java.util.ArrayList;

@SpringBootApplication
public class ToDoListApplication implements CommandLineRunner{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TestTaskRepository testTaskRepository;

	public static void main(String[] args) {
		SpringApplication.run(ToDoListApplication.class, args);
	}


    @Override
    public void run(String... args) throws Exception {
        userRepository.deleteAll();

        Task firatTask = new Task("Huawei Task", "To-Do List App", LocalDateTime.now(),false, false);
        Task firatTask2 = new Task("Huawei Task2", "To-Do List App", LocalDateTime.now(),false, false);
        Task firatTask3 = new Task("Huawei Task3", "To-Do List App", LocalDateTime.now(),false, false);
        List<Task> firatTaskList = new ArrayList<>();

        firatTaskList.add(firatTask);
        firatTaskList.add(firatTask2);
        firatTaskList.add(firatTask3);


        userRepository.save(new User("firatacar.se@gmail.com", "firatacar", "a1s2d3", firatTaskList));
        userRepository.save(new User("demetacar.se@gmail.com", "demetacar", "a1s2d3", firatTaskList));
        userRepository.save(new User("huseyinnacar.se@gmail.com", "huseyinacar", "a1s2d3", firatTaskList));


        System.out.println("fetching..");
    }
}
