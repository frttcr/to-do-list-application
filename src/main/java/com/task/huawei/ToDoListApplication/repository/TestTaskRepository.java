package com.task.huawei.ToDoListApplication.repository;

import com.task.huawei.ToDoListApplication.model.TestTask;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TestTaskRepository extends MongoRepository<TestTask, String> {
    TestTask findTaskById(String id);
}
