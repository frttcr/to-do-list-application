package com.task.huawei.ToDoListApplication.repository;

import com.task.huawei.ToDoListApplication.model.Task;
import com.task.huawei.ToDoListApplication.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

    User findUserById(String id);

}

