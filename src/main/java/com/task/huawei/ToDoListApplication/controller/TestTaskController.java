package com.task.huawei.ToDoListApplication.controller;

import com.task.huawei.ToDoListApplication.model.TestTask;
import com.task.huawei.ToDoListApplication.service.TestTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/")
public class TestTaskController {
    /**
     * Injection of TestTask Service
     */
    @Autowired
    private TestTaskService testTaskService;

    /**
     * Get all tasks
     * @return a list as json
     */
    @GetMapping("/testtasks")
    public List<TestTask> findAll() {
        return testTaskService.findAllTask();
    }

    @RequestMapping(
            method = RequestMethod.POST,
            value = "/addtask",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public TestTask addTask(@RequestBody TestTask tt) {
        return testTaskService.addTask(tt.getName(), tt.getDescription(), tt.getDeadline(), tt.getIsCompleted(), tt.getIsCompleted());
    }


    @GetMapping("/testtask/{id}")
    public TestTask getTaskById(@PathVariable String id) {
        return testTaskService.findTaskById(id);
    }


    @PutMapping("task/update/{id}")
    public TestTask updateTaskById(@PathVariable String id, @RequestBody TestTask tt) {
        return testTaskService.updateTask(id, tt.getName(), tt.getDescription(), tt.getDeadline(), tt.getIsCompleted(), tt.getIsExpired());
    }


    //@DeleteMapping("/testtask/delete/{id}")
    @RequestMapping(
            method = RequestMethod.DELETE,
            value = "/testtask/delete/{id}"
            //consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public void removeTaskById(@PathVariable String id) {
        testTaskService.deleteTask(id);
    }

}
