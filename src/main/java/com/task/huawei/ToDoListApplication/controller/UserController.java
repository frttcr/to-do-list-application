package com.task.huawei.ToDoListApplication.controller;

import com.task.huawei.ToDoListApplication.model.Task;
import com.task.huawei.ToDoListApplication.model.User;
import com.task.huawei.ToDoListApplication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("/")
public class UserController {

    /**
     * Injection of UserService
     */
    @Autowired
    private UserService userService;

    /**
     * Get all users
     * @return a list as json
     */
    @GetMapping("/users")
    public List<User> findAll() {
        return userService.findAllUser();
    }

    /**
     * Register user with email username and password
     * @param user
     * @return registered user as json
     */
    @RequestMapping(
            method = RequestMethod.POST,
            value = "/register",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public User registerUser(@RequestBody User user) {
        return userService.register(user.getEmail(), user.getUsername(), user.getPassword());
    }

    /**
     * Get user by id
     * @param id of user
     * @return an user as json
     */
    @GetMapping("/user/{id}")
    public User getUserById(@PathVariable String id) {
        return userService.findUserById(id);
    }

    /**
     * Update user account by id
     * @param id of user
     * @param email of user
     * @param username of user
     * @param password of user
     * @return an updated user as json
     */
    @PutMapping("user/update/{id}")
    public User updateUserById(@PathVariable String id, @RequestParam String email, @RequestParam String username,
                               @RequestParam String password) {
        return userService.updateUser(id, email, username, password);
    }

    /**
     * Delete user by id
     * @param id of user
     */
    @DeleteMapping("/user/delete/{id}")
    public void removeUserById(@PathVariable String id) {
        userService.deleteUser(id);
    }

    /**
     * Add task to user
     * @param id of user
     * @param name of task
     * @param description of task
     * @param deadline of task
     * @param completed status of task
     * @param expired status of task
     * @return an user with added new task
     */
    @PostMapping("/user/addtask/{id}")
    public User addTaskToUser(@PathVariable String id, @RequestParam String name, @RequestParam String description,
                              @RequestParam String deadline, @RequestParam boolean completed,
                              @RequestParam boolean expired){
        DateTimeFormatter dateTimeFormat = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        LocalDateTime deadlineDate = dateTimeFormat.parse(deadline, LocalDateTime::from);

        Task task = new Task(name, description, deadlineDate, completed, expired);

        return userService.addTask(id, task);
    }

}