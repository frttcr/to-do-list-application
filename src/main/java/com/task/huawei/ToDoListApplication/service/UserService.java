package com.task.huawei.ToDoListApplication.service;

import com.task.huawei.ToDoListApplication.model.Task;
import com.task.huawei.ToDoListApplication.repository.UserRepository;
import com.task.huawei.ToDoListApplication.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    /**
     * Injection of UserRepository
     */
    @Autowired
    private UserRepository userRepository;

    // List all users
    public List<User> findAllUser() {
        return userRepository.findAll();
    }

    // Register user
    public User register(String email, String username, String password) {
        return userRepository.save(new User(email, username, password));
    }

    public User findUserById(String id) {
        return userRepository.findUserById(id);
    }

    // Update user information
    public User updateUser(String id, String email, String username, String password){
        User user = userRepository.findUserById(id);
        user.setEmail(email);
        user.setUsername(username);
        user.setPassword(password);

        return userRepository.save(user);
    }

    // Add task to user
    public User addTask(String id, Task task) {
        User user = userRepository.findUserById(id);
        user.getTasks().add(task);
        return userRepository.save(user);
    }

    // Delete user account with id
    public void deleteUser(String id) {
        User user = userRepository.findUserById(id);
        userRepository.delete(user);
    }
}
