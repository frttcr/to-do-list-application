package com.task.huawei.ToDoListApplication.service;

import com.task.huawei.ToDoListApplication.model.TestTask;
import com.task.huawei.ToDoListApplication.repository.TestTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TestTaskService {

    @Autowired
    private TestTaskRepository testTaskRepository;

    // List all tasks
    public List<TestTask> findAllTask() {
        return testTaskRepository.findAll();
    }

    // Add Task
    public TestTask addTask(String name, String description, String deadline, String isCompleted, String isExpired) {
        return testTaskRepository.save(new TestTask(name, description, deadline, isCompleted, isExpired));
    }

    public TestTask findTaskById(String id) {
        return testTaskRepository.findTaskById(id);
    }

    // Update task's information
    public TestTask updateTask(String id,String name, String description, String deadline, String isCompleted, String isExpired){
        TestTask testTask = testTaskRepository.findTaskById(id);
        testTask.setName(name);
        testTask.setDescription(description);
        testTask.setDeadline(deadline);
        testTask.setCompleted(isCompleted);
        testTask.setExpired(isExpired);

        return testTaskRepository.save(testTask);
    }

    // Delete task with id
    public void deleteTask(String id) {
        TestTask task = testTaskRepository.findTaskById(id);
        testTaskRepository.delete(task);
    }
}
