package com.task.huawei.ToDoListApplication.model;

import java.time.LocalDateTime;

public class Task {

    private String name;
    private String description;
    private LocalDateTime createdDate;
    private LocalDateTime deadline;
    private boolean isCompleted;
    private boolean isExpired;

    public Task(String name, String description, LocalDateTime deadline, boolean isCompleted, boolean isExpired){
        this.name = name;
        this.description = description;
        this.createdDate = LocalDateTime.now();
        this.deadline = deadline;
        this.isCompleted = isCompleted;
        this.isExpired = isExpired;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public boolean isExpired() {
        return isExpired;
    }

    public void setExpired(boolean expired) {
        isExpired = expired;
    }

}
