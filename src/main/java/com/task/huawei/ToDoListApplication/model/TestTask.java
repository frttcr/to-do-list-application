package com.task.huawei.ToDoListApplication.model;

import org.apache.tomcat.jni.Local;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "TestTask")
public class TestTask {

    @Id
    private String id;
    private String name;
    private String description;
    private LocalDateTime createdDate;
    private String deadline;
    private String isCompleted;
    private String isExpired;

    public TestTask() {

    }

    public TestTask(String name, String description, String deadline, String isCompleted, String isExpired){
        this.name = name;
        this.description = description;
        this.createdDate = LocalDateTime.now();
        this.deadline = deadline;
        this.isCompleted = isCompleted;
        this.isExpired = isExpired;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getIsCompleted() {
        return isCompleted;
    }

    public void setCompleted(String completed) {
        isCompleted = completed;
    }

    public String getIsExpired() {
        return isExpired;
    }

    public void setExpired(String expired) {
        isExpired = expired;
    }
}
